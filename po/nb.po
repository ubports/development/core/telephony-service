# Norwegian Bokmal translation for telephony-service
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the telephony-service package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: telephony-service\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-16 17:03+0100\n"
"PO-Revision-Date: 2023-02-07 09:55+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/lomiri/"
"telephony-service/nb_NO/>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16-dev\n"
"X-Launchpad-Export-Date: 2017-01-04 06:03+0000\n"

#: indicator/telephony-service-call.desktop.in:4
msgid "Phone Calls"
msgstr "Telefonsamtaler"

#: indicator/telephony-service-sms.desktop.in:4
msgid "SMS"
msgstr "SMS"

#: approver/approver.cpp:66
msgid "Unknown caller"
msgstr "Ukjent anropsperson"

#: approver/approver.cpp:94
msgid "I'm busy at the moment. I'll call later."
msgstr "Jeg er opptatt for øyeblikket. Ringer deg senere."

#: approver/approver.cpp:95
msgid "I'm running late, on my way now."
msgstr "Jeg blir forsinket. Er på vei nå."

#: approver/approver.cpp:96
msgid "Please call me back later."
msgstr "Ring meg heller senere."

#: approver/approver.cpp:457
#, qt-format
msgid "On [%1]"
msgstr "På [%1]"

#: approver/approver.cpp:461 indicator/messagingmenu.cpp:414
msgid "Private number"
msgstr "Skjult nummer"

#: approver/approver.cpp:464 indicator/messagingmenu.cpp:244
#: indicator/messagingmenu.cpp:418 indicator/textchannelobserver.cpp:640
msgid "Unknown number"
msgstr "Ukjent nummer"

#: approver/approver.cpp:470 approver/approver.cpp:485
msgid "Caller number is not available"
msgstr "Anropsnummer er ikke tilgjengelig"

#: approver/approver.cpp:476
msgid "Calling from private number"
msgstr "Ringer fra skjult nummer"

#: approver/approver.cpp:479
msgid "Calling from unknown number"
msgstr "Ringer fra ukjent nummer"

#: approver/approver.cpp:482
#, qt-format
msgid "Calling from %1"
msgstr "Ringer fra %1"

#: approver/approver.cpp:529
msgid "Hold + Answer"
msgstr "Hold + svar"

#: approver/approver.cpp:530
msgid "Accept"
msgstr "Godta"

#: approver/approver.cpp:543
msgid "End + Answer"
msgstr "Avslutt + svar"

#: approver/approver.cpp:552
msgid "Decline"
msgstr "Avslå"

#: approver/approver.cpp:560
msgid "Message & decline"
msgstr "Send melding og avvis"

#: approver/main.cpp:46
msgid "Telephony Service Approver"
msgstr "Telefontjeneste-godkjenning"

#: indicator/authhandler.cpp:72
msgid "Authentication failed. Do you want to review your credentials?"
msgstr "Autentisering mislyktes. VIl du se gjennom akkreditivene dine?"

#: indicator/authhandler.cpp:91
msgid "Yes"
msgstr "Ja"

#: indicator/authhandler.cpp:94
msgid "No"
msgstr "Nei"

#: indicator/displaynamesettings.cpp:35
#, qt-format
msgid "SIM %1"
msgstr "SIM %1"

#: indicator/main.cpp:53
msgid "Telephony Service Indicator"
msgstr "Telefontjeneste-indikator"

#: indicator/messagingmenu.cpp:83 indicator/messagingmenu.cpp:87
msgid "Telephony Service"
msgstr "Telefontjeneste"

#: indicator/messagingmenu.cpp:151 indicator/messagingmenu.cpp:293
#: indicator/messagingmenu.cpp:360
msgid "Send"
msgstr "Send"

#. TRANSLATORS : %1 is the group name and %2 is the recipient name
#: indicator/messagingmenu.cpp:262 indicator/textchannelobserver.cpp:671
#: indicator/textchannelobserver.cpp:675
#, qt-format
msgid "Message to %1 from %2"
msgstr "Melding til %1 fra %2"

#. TRANSLATORS : %1 is the recipient name
#: indicator/messagingmenu.cpp:265 indicator/messagingmenu.cpp:269
#, fuzzy, qt-format
#| msgid "Message to group from %1"
msgid "Message to the group from %1"
msgstr "Melding til gruppe fra %1"

#: indicator/messagingmenu.cpp:345
msgid "Call back"
msgstr "Ring tilbake"

#: indicator/messagingmenu.cpp:350
#, fuzzy
#| msgid "I missed your call - can you call me now?"
msgid "Missed your call — can you call me now?"
msgstr "Fikk ikke tatt telefonen. Kan du ringe meg nå?"

#: indicator/messagingmenu.cpp:351
#, fuzzy
#| msgid "I'm running late. I'm on my way."
msgid "Running late. On my way."
msgstr "Jeg blir noe forsinket. Er på vei."

#: indicator/messagingmenu.cpp:352
#, fuzzy
#| msgid "I'm busy at the moment. I'll call you later."
msgid "Busy at the moment. Will call you later."
msgstr "Jeg er opptatt akkurat nå. Ringer deg senere."

#: indicator/messagingmenu.cpp:353
#, fuzzy
#| msgid "I'll be 20 minutes late."
msgid "I am 20 minutes late."
msgstr "Jeg blir 20 minutter forsinket."

#: indicator/messagingmenu.cpp:354
#, fuzzy
#| msgid "Sorry, I'm still busy. I'll call you later."
msgid "Still busy. Will call you later."
msgstr "Beklager, men jeg er fortsatt opptatt. Ringer deg senere."

#: indicator/messagingmenu.cpp:409
#, qt-format
msgid "%1 missed call"
msgid_plural "%1 missed calls"
msgstr[0] "%1 ubesvart anrop"
msgstr[1] "%1 ubesvarte anrop"

#: indicator/messagingmenu.cpp:492
msgid "Voicemail messages"
msgstr "Talemeldinger"

#: indicator/messagingmenu.cpp:495
#, qt-format
msgid "%1 voicemail message"
msgid_plural "%1 voicemail messages"
msgstr[0] "%1 talemelding"
msgstr[1] "%1 talemeldinger"

#: indicator/messagingmenu.cpp:500
msgid "Voicemail"
msgstr "Mobilsvar"

#: indicator/metrics.cpp:42
#, qt-format
msgid "<b>%1</b> text messages sent today"
msgstr "Sendt <b>%1</b> tekstmeldinger i dag"

#: indicator/metrics.cpp:43
msgid "No text messages sent today"
msgstr "Ingen sendte tekstmeldinger i dag"

#: indicator/metrics.cpp:44
#, qt-format
msgid "<b>%1</b> text messages received today"
msgstr "Mottatt <b>%1</b> tekstmeldinger i dag"

#: indicator/metrics.cpp:45
msgid "No text messages received today"
msgstr "Ingen mottatte tekstmeldinger i dag"

#: indicator/metrics.cpp:46
#, qt-format
msgid "<b>%1</b> calls received today"
msgstr "Mottatt <b>%1</b> anrop i dag"

#: indicator/metrics.cpp:47
msgid "No calls received today"
msgstr "Ingen mottatte anrop i dag"

#: indicator/metrics.cpp:48
#, qt-format
msgid "<b>%1</b> calls made today"
msgstr "Ringt ut <b>%1</b> ganger i dag"

#: indicator/metrics.cpp:49 indicator/metrics.cpp:51
msgid "No calls made today"
msgstr "Ingen utgående anrop i dag"

#: indicator/metrics.cpp:50
#, qt-format
msgid "Spent <b>%1</b> minutes in calls today"
msgstr "Vært i anrop i <b>%1</b> minutter i dag"

#: indicator/textchannelobserver.cpp:225
msgid "Unlock your sim card and try again from the messaging application."
msgstr "Lås opp SIM-kortet og prøv på nytt fra meldingsprogrammet."

#: indicator/textchannelobserver.cpp:227
msgid "Deactivate flight mode and try again from the messaging application."
msgstr "Slå av flymodus og prøv igjen fra meldingsprogrammet."

#: indicator/textchannelobserver.cpp:230
msgid "Try again from the messaging application."
msgstr "Prøv på nytt fra meldingsprogrammet."

#: indicator/textchannelobserver.cpp:236
msgid "The message could not be sent"
msgstr "Meldinga ble ikke sendt"

#: indicator/textchannelobserver.cpp:249 indicator/textchannelobserver.cpp:713
msgid "View message"
msgstr "Vis melding"

#: indicator/textchannelobserver.cpp:295
msgid "New Group"
msgstr "Ny gruppe"

#. TRANSLATORS : %1 is the group name
#: indicator/textchannelobserver.cpp:302
#, qt-format
msgid "You joined group %1 "
msgstr "Du tok del i gruppe %1 "

#. TRANSLATORS : %1 is the group name
#: indicator/textchannelobserver.cpp:306
#, qt-format
msgid "You joined group %1"
msgstr "Du tok del i gruppe %1"

#: indicator/textchannelobserver.cpp:309
msgid "You joined a new group"
msgstr "Du tok del i en ny gruppe"

#: indicator/textchannelobserver.cpp:328
msgid "View Group"
msgstr "Vis gruppe"

#: indicator/textchannelobserver.cpp:367 indicator/ussdindicator.cpp:116
msgid "Ok"
msgstr "OK"

#: indicator/textchannelobserver.cpp:373
msgid "Save"
msgstr "Lagre"

#: indicator/textchannelobserver.cpp:468
msgid "Emergency Alert"
msgstr "Nødalarm"

#: indicator/textchannelobserver.cpp:471
#, fuzzy
msgid "Emergency Alert: Extreme"
msgstr "Nødalarm: Ekstrem"

#: indicator/textchannelobserver.cpp:474
msgid "Emergency Alert: Severe"
msgstr "Nødalarm: Alvorlig"

#: indicator/textchannelobserver.cpp:477
msgid "Emergency Alert: Notice"
msgstr "Nødalarm: Merknad"

#: indicator/textchannelobserver.cpp:482
#, fuzzy
msgid "AMBER Alert"
msgstr "Savnet person"

#: indicator/textchannelobserver.cpp:485
msgid "Alert"
msgstr "Alarm"

#: indicator/textchannelobserver.cpp:531
msgid "Show alert"
msgstr "Vis alarm"

#: indicator/textchannelobserver.cpp:604
#, qt-format
msgid "Attachment: %1 image"
msgid_plural "Attachments: %1 images"
msgstr[0] "Vedlegg: %1 bilde"
msgstr[1] "Vedlegg: %1 bilder"

#: indicator/textchannelobserver.cpp:606
#, qt-format
msgid "Attachment: %1 video"
msgid_plural "Attachments: %1 videos"
msgstr[0] "Vedlegg: %1 video"
msgstr[1] "Vedlegg: %1 videoer"

#: indicator/textchannelobserver.cpp:608
#, qt-format
msgid "Attachment: %1 contact"
msgid_plural "Attachments: %1 contacts"
msgstr[0] "Vedlegg: %1 kontakt"
msgstr[1] "Vedlegg: %1 kontakter"

#: indicator/textchannelobserver.cpp:610
#, qt-format
msgid "Attachment: %1 audio clip"
msgid_plural "Attachments: %1 audio clips"
msgstr[0] "Vedlegg: %1 lydklipp"
msgstr[1] "Vedlegg: %1 lydklipp"

#: indicator/textchannelobserver.cpp:612
#, qt-format
msgid "Attachment: %1 file"
msgid_plural "Attachments: %1 files"
msgstr[0] "Vedlegg: %1 fil"
msgstr[1] "Vedlegg: %1 filer"

#. TRANSLATORS : message displayed when any error occurred while receiving a MMS (case when cellular-data is off, or any downloading issue). Notify that there was a message, the user can find more about it in the messaging-app.
#: indicator/textchannelobserver.cpp:615
#, fuzzy
#| msgid "View message"
msgid "New MMS message"
msgstr "Vis melding"

#. TRANSLATORS : %1 is the recipient name
#: indicator/textchannelobserver.cpp:679 indicator/textchannelobserver.cpp:683
#, qt-format
msgid "Message to group from %1"
msgstr "Melding til gruppe fra %1"

#: indicator/textchannelobserver.cpp:749
#, qt-format
msgid "Message from %1"
msgstr "Melding fra %1"

#: indicator/textchannelobserver.cpp:910
msgid "Please, select a SIM card:"
msgstr "Velg et SIM-kort:"

#: indicator/ussdindicator.cpp:119
msgid "Reply"
msgstr "Svar"

#: indicator/ussdindicator.cpp:142
msgid "Cancel"
msgstr "Avbryt"

#: libtelephonyservice/callnotification.cpp:53
msgid "Conf call on hold"
msgstr "Konf.-samtale på vent"

#: libtelephonyservice/callnotification.cpp:53
#, qt-format
msgid "%1 call on hold"
msgstr "%1 samtale på vent"

#: libtelephonyservice/callnotification.cpp:56
msgid "Conf call ended"
msgstr "Konf.-samtale over"

#: libtelephonyservice/callnotification.cpp:56
#, qt-format
msgid "%1 call ended"
msgstr "%1 samtale over"

#: libtelephonyservice/callnotification.cpp:59
#, qt-format
msgid "%1 call declined"
msgstr "%1 anrop avslått"

#: libtelephonyservice/contactwatcher.cpp:157
msgid "Private Number"
msgstr "Skjult nummer"

#: libtelephonyservice/contactwatcher.cpp:159
msgid "Unknown Number"
msgstr "Ukjent nummer"

#, fuzzy
#~| msgid "Telephony Service"
#~ msgid "telephony-service-call"
#~ msgstr "Telefontjeneste"

#, fuzzy
#~| msgid "Telephony Service"
#~ msgid "telephony-service-message"
#~ msgstr "Telefontjeneste"

#~ msgid ""
#~ "Oops, there has been an error with the MMS system and this message could "
#~ "not be retrieved. Please ensure Cellular Data is ON and MMS settings are "
#~ "correct, then ask the sender to try again."
#~ msgstr ""
#~ "Beklager, det har oppstått en feil med MMS-systemet, og denne meldingen "
#~ "kunne ikke hentes. Forsikre deg om at mobildata er PÅ og MMS-"
#~ "innstillinger er riktige, og be deretter avsenderen om å prøve igjen."
